# Weather-mapper-random

## Overview
Submission for Michael Pesek. Written in mostly vanilla Javascript using NodeJs for the backend

## Instructions
The dependencies that I installed with npm are listed here: 
"dependencies": {
    "bootstrap": "^4.4.1",
    "cors": "^2.8.5",
    "express": "^4.17.1",
    "leaflet": "^1.6.0",
  },

After that I just started a local server (npm start) to allow posting the info I got from openweathermap api.


