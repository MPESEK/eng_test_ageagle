
let apiKey = '64cfad6bdd5f1997dffa2fa3aa512019';
let baseUrl = 'http:/api.openweathermap.org/data/2.5/weather?lat='
let baseTwo = 'http://localhost:3000/all'
let baseThree = 'https://api.random.org/json-rpc/2/invoke'

let check = 0 // if this is 0 we haven't clicked generate yet
data = {   // can change these before api call
    "jsonrpc": "2.0",
    "method": "generateIntegers",
    "params": {
        "apiKey": "d131fb1d-dd76-4d1f-ab6d-9cc030dfb4a3",
        "n": 10,
        "min": -90,
        "max": 90,
        "replacement": true
    },
    "id": 42
}
const postData = async ( url = '', data = {})=>{ // storing data to our local server
    const response = await fetch('http://localhost:3000/add', {
    method: 'POST', 
    credentials: 'same-origin', 
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify(data), // body data type must match "Content-Type" header        
  });
  try {
    const newData = await response.json()
    return newData
  }catch(error) {
  //console.log("error", error)
  }
}

const getEntry = async (baseURL, key,x,y )=>{ // this gets our weather data from openweathermap

    let url = baseURL+y+'&lon='+x+'&appid='+key
    //console.log("trying url", url)

    const res = await fetch(url)
    try {
  
      const weatherInfo = await res.json();
      //console.log("weather INFO!!!!!",weatherInfo)
        
      return weatherInfo;
    }  catch(error) {
      //console.log("error", error);
      // appropriately handle the error
    }

};
const retrieveNumbers = async (baseThree)=>{ // this gets our random numbers from random.org
    
    //const request = await fetch(baseThree,data)
    const request = await fetch(baseThree, {
        method: 'POST', 
        credentials: 'same-origin', 
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        body: JSON.stringify(data), // body data type must match "Content-Type" header        
  });
    try{
        //console.log("request1 is", request)
        const allData = await request.json()
        //console.log("all data is",allData)
        return allData
    }
    catch(error) {
        //console.log("error", error);
    }
}
const createDisplay =   (data,x,y,childDiv,i) => { // This function creates our embedded layer as well as creating headers dynamically for our maps

    var newHeader = document.getElementById("h"+i)
    // //console.log("all available data", data)
    if(newHeader == null) return
    newHeader.innerHTML = "Zoom out if necessary and to see an embedded temperature layer<br><br>Location in (lon/lat) (" + x+')' + '/('+y+')'+"<br>Temperature: "+String((parseFloat(data.temp)-273.15).toFixed(1))+"'C" +"<br>  Temperature feels like: "+String((parseFloat(data.feels)-273.15).toFixed(1))+"'C"+ "<br>  Description: "+data.desc+ "<br>  Wind Speed: "+data.wind+" m/s" +"<br>Name (if available): " + data.name + "<br>"
    document.getElementById(childDiv.id).innerHTML = "";
    
    var mymap = L.map(childDiv.id).setView([x, y], 8);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibXBlc2VrNCIsImEiOiJjazVucG94dXEweGNpM2pwZnJiZnowdnJhIn0.F-_SA46MXsxJFZYaxSZphg', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        accessToken: 'your.mapbox.access.token'
    }).addTo(mymap);

    L.tileLayer('https://tile.openweathermap.org/map/temp_new/{z}/{x}/{y}.png?appid=64cfad6bdd5f1997dffa2fa3aa512019', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        accessToken: 'your.mapbox.access.token'
    }).addTo(mymap);   


}
const beginCallsToAPI = async (data) => { // loop through however many maps we need to call weather api for that location and handle posting
    var childDivs = document.querySelectorAll('div[id^="mapid"]');
    var j = childDivs.length+1
    //console.log("child divs",childDivs)
    
    //console.log(j)
    for (let i = 0;i<j;i++){
            //console.log("i is",i)
            var childDiv = childDivs[i-1];
            x = data.result.random.data[i] * 2
            y = data.result.random.data[i+1] 

            await getEntry(baseUrl,apiKey,x,y) // need await or for loop will leave us in the dust
                 .then(function(data){
                    postData('http://localhost:3000/add',{temp: data.main.temp,desc: data.weather[0].description, name: data.name, feels: data.main.feels_like, wind: data.wind.speed})
                    .then(function(data){
                        createDisplay(data,x,y,childDiv,i)
                    })
                })
        }
}
document.getElementById("generate").addEventListener("click", function(){ // event handler for the generate button
    
    const user_response = document.getElementById('feelings').value;
    //document.getElementById('generate').style.display = 'block';   // maps don't allow for 
    //this.style.display = 'none'

    //console.log("user response is", user_response)

    data.params.n = user_response * 2
    
    var i
    if(check> 0){  // global variable check is set to 1 after maps are populated so that we can remove them on the next generation
        var div = document.getElementById('content')
        while(div.firstChild){
            div.removeChild(div.firstChild);
        }
        //console.log("done")
    }
    for(i = 0;i <user_response;i++){ // Based on user input, we dynamically create that many divs for map and header content
        //console.log("i is", i)
        var newMap = document.createElement('div')
        var name = "mapid"+i
        newMap.setAttribute("id",name)
        var mapHead = document.createElement("HEADER")
        mapHead.setAttribute("id",'h'+i)
        var content = document.getElementById("content")  
        document.getElementById("content").appendChild(newMap)
        content.insertBefore(mapHead,newMap)
    }
    retrieveNumbers(baseThree)
    .then(function(data){
        beginCallsToAPI(data)
    })
    check = 1 // allows for re-generation of maps as commented above     
});

