const cors = require('cors');
const bodyParser = require('body-parser');
const port = 3000

const weatherData = [] //posts end up here
    
// Require Express to run server and routes
const express = require('express');
const app = express();
// Cors for cross origin allowance
app.use(cors());
// Start up an instance of app

/* Middleware*/
//Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('website'));

const server = app.listen(port, listening); //setup server

function listening(){
    console.log('server running');
    console.log(`running on localhost: ${port}`)
}

app.get('/all', function (req, res) {
    console.log(weatherData)
    res.send(weatherData);
  })
app.post('/add', addEntry); // this endpoints stores data from openweathermap 

  function addEntry(req,res){
    console.log("req body is",req.body)
    newEntry = {  
      temp: req.body.temp,
      desc: req.body.desc,
      name: req.body.name,
      feels:req.body.feels,
      wind: req.body.wind
    } 
    weatherData.push(newEntry);
    res.send(newEntry)
     
  }


